package com.snapsofts.tutorialdesign.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.snapsofts.tutorialdesign.R;
import com.snapsofts.tutorialdesign.objects.Item;

public class ListViewAdapter extends ArrayAdapter<Item> {
	private int layoutId;
	private Activity activity;
	private ArrayList<Item> arrayListItem;
	private ImageView imvListViewItem;
	private TextView tvListViewTitle;
	private TextView tvListViewDiscription;
	private TextView tvListViewDownload;
	private TextView tvListViewDateTime;

	public ListViewAdapter(Activity activity, int resource, ArrayList<Item> arrayListItem) {
		super(activity, resource, arrayListItem);
		
		this.activity = activity;
		this.arrayListItem = arrayListItem;
		this.layoutId = resource;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(layoutId, null);
		}
		tvListViewTitle = (TextView) convertView.findViewById(R.id.tvListViewTitle);
		tvListViewDiscription = (TextView) convertView.findViewById(R.id.tvListViewdDiscription);
		tvListViewDownload = (TextView) convertView.findViewById(R.id.tvListViewDownload);
		tvListViewDateTime = (TextView) convertView.findViewById(R.id.tvListViewDateTime);
		imvListViewItem = (ImageView) convertView.findViewById(R.id.imvListViewItem);
		
		imvListViewItem.setBackgroundResource(arrayListItem.get(position).getImvItem());
		tvListViewTitle.setText(arrayListItem.get(position).getTitle());
		tvListViewDiscription.setText(arrayListItem.get(position).getDiscription());
		tvListViewDownload.setText(arrayListItem.get(position).getDonwnload());
		tvListViewDateTime.setText(arrayListItem.get(position).getDatetime());
		
		return convertView;
	}
	
}
