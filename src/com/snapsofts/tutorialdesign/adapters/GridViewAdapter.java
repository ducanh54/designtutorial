package com.snapsofts.tutorialdesign.adapters;

import java.util.ArrayList;

import com.snapsofts.tutorialdesign.R;
import com.snapsofts.tutorialdesign.common.TutorialDesignUtils;
import com.snapsofts.tutorialdesign.objects.Item;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewAdapter extends ArrayAdapter<Item> {
	private int layoutId;
	private ArrayList<Item> arrayListItem;
	private Activity activity;
	private ImageView imvGridViewItem;
	private TextView tvGridViewDownload;
	
	public GridViewAdapter(Activity activity, int resource, ArrayList<Item> arrayListItem) {
		super(activity, resource, arrayListItem);
		this.activity = activity;
		this.layoutId = resource;
		this.arrayListItem = arrayListItem;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			convertView = inflater.inflate(layoutId, null);
		}
		
		imvGridViewItem = (ImageView) convertView.findViewById(R.id.imvGridViewItem);
		tvGridViewDownload = (TextView) convertView.findViewById(R.id.tvGridViewDownload);
		Bitmap imageView = BitmapFactory.decodeResource(activity.getResources(), arrayListItem.get(position).getImvItem());
		imvGridViewItem.setImageBitmap(TutorialDesignUtils.getRoundeCornerBitmap(imageView, 10));
		tvGridViewDownload.setText(arrayListItem.get(position).getDonwnload());
		
		return convertView;
	}
}
