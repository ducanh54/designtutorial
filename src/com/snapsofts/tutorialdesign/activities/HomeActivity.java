package com.snapsofts.tutorialdesign.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;

import com.snapsofts.tutorialdesign.R;
import com.snapsofts.tutorialdesign.adapters.GridViewAdapter;
import com.snapsofts.tutorialdesign.adapters.ListViewAdapter;
import com.snapsofts.tutorialdesign.common.TutorialDesignUtils;
import com.snapsofts.tutorialdesign.objects.Item;

public class HomeActivity extends Activity implements OnClickListener {

	private ListView listView;
	private Item item;
	private ArrayList<Item> arrayListItem;
	private ListViewAdapter listViewAdapter;

	private GridView gridView;
	private GridViewAdapter gridViewAdapter;

	private Button btnGridView;
	private Button btnListView;
	
	private View viewStatusBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_home);
		viewStatusBar = (View) findViewById(R.id.viewStatusBar);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Window window = this.getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.TRANSPARENT);
			viewStatusBar.setVisibility(View.VISIBLE);
			viewStatusBar.getLayoutParams().height = TutorialDesignUtils.getStatusBarHeight(this);
			viewStatusBar.setBackgroundColor(getResources().getColor(R.color.bg_actionbar));
		}
		
		arrayListItem = new ArrayList<Item>();
		createDataDemo();

		btnGridView = (Button) findViewById(R.id.btnGridView);
		btnListView = (Button) findViewById(R.id.btnListView);
		btnGridView.setOnClickListener(this);
		btnListView.setOnClickListener(this);
		
		listView = (ListView) findViewById(R.id.listView);
		listViewAdapter = new ListViewAdapter(this, R.layout.item_listview, arrayListItem);
		listView.setAdapter(listViewAdapter);

		gridView = (GridView) findViewById(R.id.gridView);
		gridViewAdapter = new GridViewAdapter(this, R.layout.item_gridview, arrayListItem);
		gridView.setAdapter(gridViewAdapter);

		listView.setOnItemClickListener(new ListViewOnItemClickListener());
	}

	private void createDataDemo() {
		item = new Item("Cùng cầu nguyện nào","Đầu xuân năm mới cùng cầu nguyện cùng em nào", "325","11:55 P.M, 02/23/2014", R.drawable.img_1);
		arrayListItem.add(item);
		item = new Item("Xinh", "Các bạn thấy thế nào? Có xinh không", "325","11:55 P.M, 02/23/2014", R.drawable.img_7);
		arrayListItem.add(item);
		item = new Item("Title 01", "Discription for title 01", "6969","11:55 P.M, 02/23/2014", R.drawable.img_2);
		arrayListItem.add(item);
		item = new Item("Title 02", "Discription for title 02", "6969","11:55 P.M, 02/23/2014", R.drawable.img_3);
		arrayListItem.add(item);
		item = new Item("Title 03", "Discription for title 03", "6969","11:55 P.M, 02/23/2014", R.drawable.img_4);
		arrayListItem.add(item);
		item = new Item("Title 04", "Discription for title 04", "6969","11:55 P.M, 02/23/2014", R.drawable.img_8);
		arrayListItem.add(item);
		item = new Item("Cùng cầu nguyện nào","Đầu xuân năm mới cùng cầu nguyện cùng em nào", "325","11:55 P.M, 02/23/2014", R.drawable.img_5);
		arrayListItem.add(item);
		item = new Item("Xinh", "Các bạn thấy thế nào? Có xinh không", "325","11:55 P.M, 02/23/2014", R.drawable.img_6);
		arrayListItem.add(item);
		item = new Item("Title 01", "Discription for title 01", "6969","11:55 P.M, 02/23/2014", R.drawable.img_9);
		arrayListItem.add(item);
		item = new Item("Title 02", "Discription for title 02", "6969","11:55 P.M, 02/23/2014", R.drawable.img_10);
		arrayListItem.add(item);
		item = new Item("Title 03", "Discription for title 03", "6969","11:55 P.M, 02/23/2014", R.drawable.img_1);
		arrayListItem.add(item);
		item = new Item("Title 04", "Discription for title 04", "6969","11:55 P.M, 02/23/2014", R.drawable.img_2);
		arrayListItem.add(item);
		item = new Item("Cùng cầu nguyện nào","Đầu xuân năm mới cùng cầu nguyện cùng em nào", "325","11:55 P.M, 02/23/2014", R.drawable.img_1);
		arrayListItem.add(item);
		item = new Item("Xinh", "Các bạn thấy thế nào? Có xinh không", "325","11:55 P.M, 02/23/2014", R.drawable.img_7);
		arrayListItem.add(item);
		item = new Item("Title 01", "Discription for title 01", "6969","11:55 P.M, 02/23/2014", R.drawable.img_2);
		arrayListItem.add(item);
		item = new Item("Title 02", "Discription for title 02", "6969","11:55 P.M, 02/23/2014", R.drawable.img_3);
		arrayListItem.add(item);
		item = new Item("Title 03", "Discription for title 03", "6969","11:55 P.M, 02/23/2014", R.drawable.img_4);
		arrayListItem.add(item);
		item = new Item("Title 04", "Discription for title 04", "6969","11:55 P.M, 02/23/2014", R.drawable.img_8);
		arrayListItem.add(item);
		item = new Item("Cùng cầu nguyện nào","Đầu xuân năm mới cùng cầu nguyện cùng em nào", "325","11:55 P.M, 02/23/2014", R.drawable.img_5);
		arrayListItem.add(item);
		item = new Item("Xinh", "Các bạn thấy thế nào? Có xinh không", "325","11:55 P.M, 02/23/2014", R.drawable.img_6);
		arrayListItem.add(item);
		item = new Item("Title 01", "Discription for title 01", "6969","11:55 P.M, 02/23/2014", R.drawable.img_9);
		arrayListItem.add(item);
		item = new Item("Title 02", "Discription for title 02", "6969","11:55 P.M, 02/23/2014", R.drawable.img_10);
		arrayListItem.add(item);
		item = new Item("Title 03", "Discription for title 03", "6969","11:55 P.M, 02/23/2014", R.drawable.img_1);
		arrayListItem.add(item);
		item = new Item("Title 04", "Discription for title 04", "6969","11:55 P.M, 02/23/2014", R.drawable.img_2);
		arrayListItem.add(item);
	}

	@Override
	public void onClick(View v) {
		if (v == btnGridView) {
			listView.setVisibility(View.GONE);
			gridView.setVisibility(View.VISIBLE);
		}

		if (v == btnListView) {
			gridView.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}
	}

	public class ListViewOnItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
			intent.putExtra("title", arrayListItem.get(position).getTitle());
			startActivity(intent);
		}
	}
	
}
