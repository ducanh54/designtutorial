package com.snapsofts.tutorialdesign.activities;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.snapsofts.tutorialdesign.R;
import com.snapsofts.tutorialdesign.common.TutorialDesignUtils;

public class DetailActivity extends Activity implements OnClickListener {
	private ImageButton btnBack;
	private String title;
	private TextView tvDetailTitle;
	private View viewDetailStatusBar;
	private View viewDetailNavigationBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_detail);
		
		viewDetailStatusBar = (View) findViewById(R.id.viewDetailStatusBar);
		viewDetailNavigationBar = (View) findViewById(R.id.viewDetailNavigationBar);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Window window = this.getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(Color.TRANSPARENT);
			viewDetailStatusBar.setVisibility(View.VISIBLE);
			viewDetailStatusBar.getLayoutParams().height = TutorialDesignUtils.getStatusBarHeight(this);
			viewDetailStatusBar.setBackgroundResource(R.drawable.bg_actionbar_details);
			viewDetailNavigationBar.setVisibility(View.VISIBLE);
			viewDetailNavigationBar.getLayoutParams().height = TutorialDesignUtils.getNavigationBar(this);
			viewDetailNavigationBar.setBackgroundResource(R.drawable.bg_actionbar_details);
		}
		
		btnBack = (ImageButton) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(this);
		tvDetailTitle = (TextView) findViewById(R.id.tvDetailTitle);
		
		Bundle bundle = getIntent().getExtras();
		if(bundle != null) {
			title = bundle.getString("title");
			tvDetailTitle.setText(title);
		}
	}

	@Override
	public void onClick(View v) {
		if (v == btnBack) {
			finish();
		}
	}
	
}
