package com.snapsofts.tutorialdesign.objects;


public class Item {
	
	private String title;
	private String discription;
	private String donwnload;
	private String datetime;
	private Integer imvItem;

	public Item() {
	}

	public Item(String title, String discription, String donwnload,
			String datetime, Integer imvItem) {
		super();
		this.title = title;
		this.discription = discription;
		this.donwnload = donwnload;
		this.datetime = datetime;
		this.imvItem = imvItem;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public String getDonwnload() {
		return donwnload;
	}

	public void setDonwnload(String donwnload) {
		this.donwnload = donwnload;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public Integer getImvItem() {
		return imvItem;
	}

	public void setImvItem(Integer imvItem) {
		this.imvItem = imvItem;
	}

}
